<?php


namespace JaPhIM\lib\core;


 interface HistoryAbstract
{
    function getHistory($offset, $num);

    function addHistory($user_id, $another_id, $msg);
}