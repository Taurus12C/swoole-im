<?php


namespace JaPhIM\lib\log;


interface ILogHandler
{
    public function write($msg);
}