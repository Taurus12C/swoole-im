# SwooleIM

#### 介绍
一款基于 swoole的聊天工具的服务端(即时通讯)，支持发送图片文件、历史记录等，类似WEB微信，可根据自己需求进行扩展更多丰富的功能

#### 软件架构
软件架构说明
~~~
|—— src                       
    |——lib
        |——core             核心处理文件
        |——db               数据库连接池
        |——handle           主要业务处理方法性状
        |——log              日志处理
    |——Server.php           服务基类
|——config.php               配置文件
|——index.php                启动文件
~~~
#### 安装教程
>目前仅支持下载使用，后续待完善

#### 使用说明

>启动方法与swoole保持一致，切换到根目录使用 `php index.php`执行即可。**本项目所需扩展与swoole保持一致,使用前请导入`/src/lib/db/swooleim.sql`数据库,并且使用composer安装本项目所需依赖**

1. 可以在`/src/lib/handle/OnMessage.php`中添加自定义的业务逻辑处理方法，
   方法名对应前端`send()`数据中的`'type'`字段，示例：
    ```javascript
    ws = new WebSocket(url);
    let data = {
        'type' : 'online',
        ...
    };
    ws.send(JSON.stringify(data));
    ```
    
    ```php
    /*
     * 此时上述send对应的处理方法为/src/lib/handle/OnMessage.php
     * 中的 function online(){}方法
     */
    private function online($server,  $frame){
        $msg = json_decode($frame->data,true);
        $server->bind($frame->fd,$msg['user_id']);
        $this->core->online($msg['user_id']);
        $this->broadcast([
            'type' => 'online',
            'user_id' => $msg['user_id']
        ]);
    }
    ```
    
2. `/src/lib/core/handle.php`中均为处理与数据库等服务交互的方法，
   例如聊天记录，用户的在线情况等。在`OnMessage.php`中可以使用
   `$this->core->function()`调用,**core中的数据库使用连接池实现，使用
   `$db = $this->db->get();`获取，处理完逻辑后请使用`$this->db->put($db);`
   归还**
3. 全局可以使用`Log::DEBUG("DEBUG");Log::WARN("WARN");Log::ERROR("ERROR");Log::INFO("INFO");`
   方法来记录日志
 
#### 参与贡献

1.  Fork 本仓库
2.  新建 Feat_xxx 分支
3.  提交代码
4.  新建 Pull Request


#### 码云特技

1.  使用 Readme\_XXX.md 来支持不同的语言，例如 Readme\_en.md, Readme\_zh.md
2.  码云官方博客 [blog.gitee.com](https://blog.gitee.com)
3.  你可以 [https://gitee.com/explore](https://gitee.com/explore) 这个地址来了解码云上的优秀开源项目
4.  [GVP](https://gitee.com/gvp) 全称是码云最有价值开源项目，是码云综合评定出的优秀开源项目
5.  码云官方提供的使用手册 [https://gitee.com/help](https://gitee.com/help)
6.  码云封面人物是一档用来展示码云会员风采的栏目 [https://gitee.com/gitee-stars/](https://gitee.com/gitee-stars/)
