<?php
/*
 * 配置文件
 */
return [
    //聊天服务器配置
    'server' => [
        'host' => '0.0.0.0',            //host,一般无需改动
        'port' => '8282',               //服务监听的端口号，外网访问注意开放服务器的端口及防火墙
    ],

    //swoole服务配置
    'swoole_server' => [
        'log_file'                  => __DIR__.'/log/swoole.log',       //运行的日志文件
        'log_level'                 => 'SWOOLE_LOG_DEBUG',              //日志级别
        'reactor_num'               => swoole_cpu_num()*2,              //启动的reactor线程数
        'worker_num'                => swoole_cpu_num(),                //启动的进程数
        'max_request'               => 10000,                           //进程的最大任务数量
        'task_worker_num'           => 0,                               //Task进程数量
        'heartbeat_check_interval'  => 60,                              //心跳检测
        'heartbeat_idle_time'       => 180,                             //允许的最大空闲时间
        'ssl_cert_file'             => '',                              //cert证书路径
        'ssl_key_file'              => '',                              //key私钥路径
        'daemonize'                 => 0,                               //是否启动守护进程
        'hook_flags'                => SWOOLE_HOOK_ALL,                 //协程
    ],

    //数据库配置，使用Medoo/Medoo数据库操作包，请自行composer
    'database' => [
        'database_type' => 'mysql',             //数据库类型
        'database_name' => 'swooleim',          //数据库名
        'server' => '127.0.0.1',                //IP
        'port' => 3306,                         //端口
        'username' => 'root',
        'password' => 'root',
        'charset' => 'utf8',
        'prefix' => 'swooleim_',                         //表前缀
    ],

    'cache' => [
        'cache_log' => __DIR__.'/runtime/'
    ],

    //日志文件路径
    'file_log' => [
        'log_dir'   => __DIR__.'/log/'
    ]

];
